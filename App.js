import React, { Component } from 'react';
import Main from './src/Main';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import cfgStore, { persistor } from './src/store/configureStore';

// disable warning box on production
console.disableYellowBox = true;
// cfgStore function encapsulates our store creation logic
const store = cfgStore();

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Main />
        </PersistGate>
      </Provider>
    );
  }
}
