# FreeGo - mobile
This project was bootstrapped with [Create React Native App](https://github.com/react-community/create-react-native-app).

FreeGo is an app which helps you to manage your(s) refrigerator(s) and food expiration date.

## Setup

1. Clone the repo
2. Go to repo folder then install the dependencies

```sh
$ cd mobile
$ npm install
```

3. Run your emulation device
4. Run the app

```sh
$ react-native run-android
```
