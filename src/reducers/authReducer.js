// Implementation of reducer for auth
// all actions
import {
  LOGIN,
  LOGOUT,
  SIGNUP,
  IS_LOADING,
  HAS_ERROR,
} from '../constants/actionTypes'

const initialState = {
  isLogged: false,
  hasError : false,
  isLoading: false,
};

export default (state = initialState, action) => {

  switch (action.type) {
    case HAS_ERROR:
      return {
        ...state,
        hasError: true,
        isLoading: false
      }
    case IS_LOADING:
      return {
        ...state,
        isLoading: true
      }
    case SIGNUP:
     return {
       ...state,
       isLoading: false,
       hasError: false,
     }
    case LOGIN:
      return {
        ...state,
        isLogged: true,
        isLoading: false,
        hasError: false,
      }
    case LOGOUT:
      return {
        ...state,
        isLogged: false,
        isLoading: false,
        hasError: false,
      }
    default:
      return state;
  }
};
