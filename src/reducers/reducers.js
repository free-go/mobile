import { combineReducers } from 'redux';
import authReducer from './authReducer';
import stockReducer from './stockReducer';

/* There are currently two differents reducers : auth and stock
  combineReducers helper function turns an object whose values
  are different reducing functions into a single reducing function
  you can pass to createStore
  */  
export default combineReducers({
  authReducer: authReducer,
  stockReducer: stockReducer
});
