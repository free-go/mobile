// Implementation of reducer for stock
// all actions
import {
  FETCH_STOCKS_SUCCES,
  FETCH_STOCK_SUCCESS,
  FETCH_REMOVE_STOCK_SUCCESS,
  FETCH_ADD_STOCK_SUCCESS,
  FETCH_ADD_PRODUCT_SUCCESS,
  IS_LOADING,
  FETCH_ADD_USER_SUCCESS,
  HAS_ERROR,
  LOGOUT
} from '../constants/actionTypes'

const initialState = {
  hasError : false,
  isLoading: false,
  stocks: [],
  currentStock: null,
  idStock: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGOUT:
      return {
        initialState
      }
    case FETCH_ADD_USER_SUCCESS:
      return {
        ...state,
        hasError: false,
        isLoading: false,
      }
    case FETCH_STOCKS_SUCCES:
      return {
        ...state,
        stocks: action.payload.data,
        currentStock: null,
        idStock: null,
        isLoading: false
      }
    case FETCH_STOCK_SUCCESS:
      return {
        ...state,
        currentStock: action.payload.data,
        idStock: action.idStock.idStock,
        isLoading: false
      }
    case FETCH_REMOVE_STOCK_SUCCESS:
      return {
        ...state,
        currentStock: null,
        idStock: null,
        isLoading: false
      }
    case FETCH_ADD_PRODUCT_SUCCESS:
      return {
        ...state,
        isLoading: false
      }
    case FETCH_ADD_STOCK_SUCCESS:
      return {
        ...state,
        currentStock: null,
        idStock: null,
        isLoading: false
      }
    case IS_LOADING:
      return {
        ...state,
        isLoading: true
      }
    case HAS_ERROR:
      return {
        ...state,
        hasError: true,
        isLoading: false
      }
    default:
      return state;
  }
};
