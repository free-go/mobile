import { createStore, applyMiddleware } from 'redux';
import reducers from '../reducers/reducers';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
  key: 'root',
  storage: storage
};

const cfgStore = () => {
  const middlewares = [thunk];
  // create a store enhancer
  const enhancer = applyMiddleware(...middlewares);
  const persistedReducer = persistReducer(persistConfig, reducers);

  /* pass persistReducers to the Redux create store
   function which returns a store object
  */
  return createStore(persistedReducer, enhancer);
};

export const persistor = persistStore(cfgStore());
export default cfgStore;
