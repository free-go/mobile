import { AsyncStorage } from 'react-native';
import {
  LOGIN,
  LOGOUT,
  SIGNUP,
  IS_LOGGED,
  IS_LOADING,
  HAS_ERROR
} from '../constants/actionTypes';

const URL = 'http://vps631308.ovh.net';

/**
 *  Create new account
 *  @param string email Email address of the new account
 *  @param string username username of the new account
 *  @param string password password of the new account
 *  @return dispatch method
 */
export const signUp = (email, username, password) => {
  return (dispatch) => {
    dispatch(isLoading());
    return fetch(URL + '/auth/register', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({email: email, username: username, password: password})
    })
      .then(handleErrors)
      .then(res => {
        if (res.ok) {
          dispatch(signUpSuccess());
        } return;
      })
      .catch((err) => dispatch(hasError(err)));
  }
}

/**
 *  User login method. Set the user token if
 *  @param string password password of the user
 *  @param string email Email address of the user
 *  @return dispatch method
 */
export const login = (email, password) => {
  return (dispatch) => {
    dispatch(isLoading());
    return fetch(URL + '/auth/login', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({email: email, password: password})
      })
        .then(handleErrors)
        .then((res) => res.json())
        .then(res => {
          AsyncStorage.setItem('token', res.access_token);
          dispatch(isLogged());
        })
        .catch((err) => dispatch(hasError()));
  }
}

/**
 *  Logout user
 *  Remove token
 *  @return action type LOGOUT
 */
export const logout = () => {
  AsyncStorage.removeItem('token');
  return {
    type: LOGOUT
  }
};

const handleErrors = (res) => {
  if(!res.ok)
    throw Error(res.statusText);
  return res;
}

/**
 *  Called when user is logged
 *  @return action type LOGIN
 */
const isLogged = () => {
  return {
    type: LOGIN,
  }
}

/**
 *  Called when user is signed up
 *  @return action type SIGNUP
 */
const signUpSuccess = () => {
  return {
    type: SIGNUP
  }
}

/**
 *  Loading method
 *  @return action type IS_LOADING
 */
const isLoading = () => {
  return {
    type: IS_LOADING
  }
}

/**
 *  Called when error is occured
 *  @return action type HAS_ERROR
 */
const hasError = (error) => {
  return {
    type: HAS_ERROR,
  }
}
