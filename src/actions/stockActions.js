import { AsyncStorage } from 'react-native';
import {
  FETCH_STOCKS_SUCCES,
  FETCH_REMOVE_STOCK_SUCCESS,
  FETCH_ADD_STOCK_SUCCESS,
  FETCH_STOCK_SUCCESS,
  FETCH_ADD_PRODUCT_SUCCESS,
  IS_LOADING,
  FETCH_ADD_USER_SUCCESS,
  HAS_ERROR
} from '../constants/actionTypes';

const URL = 'http://vps631308.ovh.net';

/**
  * Get all stocks of connected user
  * @return json list of stocks and dispatch method
  */
export const getStocks = () => {
  return (dispatch) => {
    dispatch(isLoading());
    AsyncStorage.getItem('token').then(token => {
      return fetch(URL + '/stock', {
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      })
        .then(handleErrors)
        .then((res) => res.json())
        .then((json) => {
          dispatch(fetchStocksSuccess(json));
          return json;
        })
        .catch(err => dispatch(hasError(err)));
    })
  }
}

/**
 *  Add user to a stock
 *  @param number idStock identifiant of stock
 *  @param number userCode identifiant of user
 *  @return dispatch method
 */
export const addUser = (idStock, userCode) => {
  return (dispatch) => {
    dispatch(isLoading());
    AsyncStorage.getItem('token').then(token => {
      return fetch(URL + '/stock/' + idStock + '/adduser', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({user_id: userCode})
      })
        .then(handleErrors)
        .then((res) => res.json())
        .then(() => {
          dispatch(fetchAddUserSuccess());
        })
        .catch(err => dispatch(hasError(err)));
    })
  }
}

/**
 *  Get stock
 *  @param number idStock identifiant of stock
 *  @return json stock's content and dispatch method
 */
export const getStock = (idStock) => {
  return (dispatch) => {
    dispatch(isLoading());
    AsyncStorage.getItem('token').then(token => {
      return fetch(URL + '/product/stock/' + idStock, {
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      })
        .then(handleErrors)
        .then((res) => res.json())
        .then(json => {
          dispatch(fetchStockSuccess(json, idStock));
          return json;
        })
        .catch(err => dispatch(hasError(err)));
    });
  }
}

/**
 *  Update quantity of product
 *  @param number idStock identifiant of stock
 *  @param number idProduct identifiant of product
 *  @param number quantity new quantity of the product
 *  @return dispatch method
 */
export const updateQuantity = (idStock, idProduct, quantity) => {
  return (dispatch) => {
    dispatch(isLoading());
    return AsyncStorage.getItem('token').then(token => {
      fetch(URL + '/product/' + idProduct + '/stock/' + idStock, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({quantity: quantity})
      })
        .then(handleErrors)
        .then(() => {
          dispatch(getStock(idStock));
        })
        .catch(err => {
          dispatch(hasError(err))});
    });
  }
}

/**
 *  Create new stock
 *  @param string name stock's name
 *  @return dispatch method
 */
export const addStock = (name) => {
  return (dispatch) => {
    dispatch(isLoading());
    return AsyncStorage.getItem('token').then(token => {
      fetch(URL + '/stock', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + token
          },
          body: JSON.stringify({name: name})
        })
          .then(handleErrors)
          .then(() => {
            dispatch(getStocks()); // update the list of stocks
            dispatch(addStockSuccess());
          })
          .catch(err => dispatch(hasError(err)));
      });
  }
}

/**
 *  Add new product to stock
 *  @param number idStock identifiant of stock
 *  @param number idProduct identifiant of product
 *  @param number quantity quantity of the new product
 *  @return dispatch method
 */
export const addProduct = (idStock, idProduct, quantity) => {
  return (dispatch) => {
    return AsyncStorage.getItem('token').then(token => {
      fetch(URL + '/product' + '/stock/' + idStock, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + token,
          },
          body: JSON.stringify({ product_id: idProduct, quantity: quantity })
      })
        .then(handleErrors)
        .then(() => {
          dispatch(getStock(idStock));
          dispatch(addProductSuccess());
          return;
        })
          .catch(err => dispatch(hasError(err)));
    });
  }
}

/**
 * Remove product from stock
 *  @param number idStock identifiant of stock
 *  @param number idProduct identifiant of product
 *  @return dispatch method
 */
export const removeProduct = (idStock, idProduct) => {
  return (dispatch) => {
    return AsyncStorage.getItem('token').then(token => {
      fetch(URL + '/product/' + idProduct + '/stock/' + idStock, {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + token,
        }
      })
    })
      .then(handleErrors)
      .then(() => {
        dispatch(getStock(idStock));
      })
      .catch(err => dispatch(hasError(err)))
  }
}

/**
 * Remove stock
 *  @param number idStock identifiant of stock
 *  @return dispatch method
 */
export const removeStock = (idStock) => {
  return (dispatch) => {
    dispatch(isLoading());
    AsyncStorage.getItem('token').then(token => {
      return fetch(URL + '/stock/' + idStock, {
          method: 'DELETE',
          headers: {
            'Authorization': "Bearer " + token,
          },
        })
          .then(handleErrors)
          .then(() => {
            dispatch(removeStockSuccess());
            return;
          })
          .catch(err => dispatch(hasError(err)));
    });
  }
}

const handleErrors = (res) => {
  if(!res.ok)
    throw Error(res.statusText);
  return res;
}

const removeStockSuccess = () => {
  return {
    type: FETCH_REMOVE_STOCK_SUCCESS
  }
}

const addStockSuccess = () => {
  return {
    type: FETCH_ADD_STOCK_SUCCESS
  }
}

const addProductSuccess = () => {
  return {
    type: FETCH_ADD_PRODUCT_SUCCESS
  }
}

const fetchStockSuccess = (data, idStock) => {
  return {
    type: FETCH_STOCK_SUCCESS,
    payload: {data},
    idStock: {idStock}
  }
}

const fetchAddUserSuccess = () => {
  return {
    type: FETCH_ADD_USER_SUCCESS
  }
}

const fetchStocksSuccess = (data) => {
  return {
    type: FETCH_STOCKS_SUCCES,
    payload: {data}
  }
}

const isLoading = () => {
  return {
    type: IS_LOADING
  }
}

const hasError = (err) => {
  return {
    type: HAS_ERROR,
    payload: err
  }
}
