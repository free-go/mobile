export const BARCODE = require('./bar-code.png');
const NUTRISCORE = {
  a: require('./nutriscore_A.png'),
  b: require('./nutriscore_B.png'),
  c: require('./nutriscore_C.png'),
  d: require('./nutriscore_D.png'),
  e: require('./nutriscore_E.png')
}

export default NUTRISCORE;
