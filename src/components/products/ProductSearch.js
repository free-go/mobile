import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { colors } from '../../theme/colors';

const ProductSearch = ({ product, onPress }) => {
  return (
    <TouchableOpacity activeOpacity={.5} style={styles.product} onPress={onPress} >
      <Text style={styles.name}>{product.name}</Text>
      <Icon name="plus-box" size={32} color={colors.primary}/>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  product: {
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  name: {
    fontSize: 20,
  },
});

export default ProductSearch;
