import React, {Component} from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, Modal } from 'react-native';

// custom components
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Input from '../commons/Input';
import Button from '../commons/Button';

import NUTRISCORE from '../../theme/images/index';
import { colors } from '../../theme/colors';

class ProductItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      newQt: '',
      modalQuantityVisible: false,
      modalDetailsVisible: false
    }
  }

  render() {
    let { product } = this.props; // from parent
    return (
      <View>
        <TouchableOpacity activeOpacity={.5} style={styles.product} onPress={() => this.setState({modalDetailsVisible: true}) }>
          <Image
            source={{uri: product.product.image}}
            style={styles.image}
            resizeMode="contain" />
          <View style={styles.info}>
            <Text style={styles.name}>{product.product.name}</Text>
            <Text style={styles.quantity}>({product.quantity})</Text>
          </View>
          <Icon name="chevron-right" size={28} color={colors.primary}/>
        </TouchableOpacity>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalDetailsVisible}
          onRequestClose={() => {}} >
          <View>
            <View style={styles.modalHeader}>
              <TouchableOpacity onPress={() => { this.setState({ modalDetailsVisible: false }) }}>
                <Icon name="close" size={35} color={colors.primary}/>
              </TouchableOpacity>
            </View>
            <View style={styles.modalDetails}>
              <Image
                source={{uri: product.product.image}}
                style={styles.modalImage}
                resizeMode="contain" />
              <View style={styles.details}>
                <Text style={styles.modalInfoName}>{product.product.name}</Text>
                <Text>{product.product.brand}</Text>
                <Image source={NUTRISCORE[product.product.nutriscore]} style={styles.modalImage} resizeMode="contain" />
              </View>
              <View style={styles.actions}>
                <Button text="MODIFIER LA QUANTITE" onPress={() => this.setState({ modalQuantityVisible: true })} />
                <Button text="RETIRER DU FRIGO" onPress={() => { this.setState({modalDetailsVisible: false}); this.props.onDelete(product.product.id) }} />
              </View>
            </View>
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalQuantityVisible}
          onRequestClose={() => {}} >
          <View>
            <View style={styles.modalHeader}>
              <TouchableOpacity onPress={() => { this.setState({ modalQuantityVisible: false }) }}>
                <Icon name="close" size={35} color={colors.primary}/>
              </TouchableOpacity>
            </View>
            <View style={styles.modalForm}>
              <Text>Nouvelle quantité: </Text>
              <Input
                placeholder="Quantité"
                keyboardType="numeric"
                onChangeText={(newQt) => this.setState({ newQt: newQt }) }
                onSubmitEditing={this._handleOnPress}
                maxLength={5}
                value={`${this.state.newQt}`} />
              <Button
                text="VALIDER"
                onPress={() => {
                  this.setState({modalQuantityVisible: false, modalDetailsVisible: false});
                  this.props.onUpdate(product.product.id, this.state.newQt)
                }}/>
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  modalDetails: {
    paddingTop: 50,
    paddingHorizontal: 20,
    justifyContent: 'center', alignItems: 'center',
  },
  details: {
    paddingBottom: 20
  },
  modalInfoName: {
    fontSize: 18,
    fontWeight: '900',
  },
  actions: {
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  product: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
    alignItems: 'center',
  },
  info: {
    flex: 1,
    flexDirection: 'column',
  },
  quantity: {
    fontSize: 14
  },
  name: {
    fontSize: 20,
  },
  modalHeader: {
    position: 'absolute',
    right: 0,
    top: 0,
  },
  modalForm: {
    paddingTop: 80,
    paddingHorizontal: 60
  },
  image: {
    width: 70,
    height: 70
  },
  modalImage: {
    width: 100,
    height: 100
  }
});

export default ProductItem;
