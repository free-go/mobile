import React from 'react';
import { StyleSheet, Image, Text } from 'react-native';

import NUTRISCORE from '../../theme/images/index';

// Nutriscore component is used to display the right image of nutriscore
const Nutriscore = ({ score }) => {
  return (
    <Image source={NUTRISCORE[score]} style={styles.image} resizeMode="contain" />
  )
}

const styles = StyleSheet.create({
  image: {
    flex:1,
    height: undefined,
    width: undefined,
  },
});

export default Nutriscore;
