import React from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';

// Loader component is used when isLoading is true
const Loader = ({ text, onPress }) => {
  return (
    <View style={styles.loading}>
      <ActivityIndicator size="large" />
    </View>
  )
}

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default Loader;
