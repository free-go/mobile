import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

import { colors } from '../../theme/colors';

const Button = ({ text, onPress }) => {
  return (
    <TouchableOpacity
      style={styles.button}
      activeOpacity={.5}
      onPress={onPress} >
        <Text
          style={styles.text}>
            {text}
        </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: colors.primary,
    padding: 15
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16
  }
});

export default Button;
