import React from 'react';
import { StyleSheet, TextInput } from 'react-native';

import { colors } from '../../theme/colors';

const Input = ({ placeholder, type, keyboardType, onChangeText, onSubmitEditing, maxLength, value, secure }) => {
  return (
    <TextInput
      style={styles.input}
      placeholder={placeholder}
      textContentType={type}
      onChangeText={onChangeText}
      keyboardType={keyboardType}
      onSubmitEditing={onSubmitEditing}
      secureTextEntry={secure}
      maxLength={maxLength}
      value={value}
    />
  )
}

const styles = StyleSheet.create({
  input: {
    height: 45,
    marginBottom: 15,
    borderBottomWidth: 1.5,
    fontSize: 18,
    borderBottomColor: colors.primary,
  }
});

export default Input;
