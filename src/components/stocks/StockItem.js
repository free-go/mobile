import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { colors } from '../../theme/colors';

const StockItem = ({ stock, onPress }) => {
  return (
    <TouchableOpacity activeOpacity={.5}
      style={styles.fridge}
      onPress={onPress} >
      <Icon name="fridge" size={60} color={colors.primary} />
      <View style={styles.info}>
        <Text style={styles.name}>{stock.name}</Text>
      </View>
      <Icon name="chevron-right" size={28} color={colors.primary} />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  fridge: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
    alignItems: 'center',
  },
  info: {
    flex: 1,
    flexDirection: 'column',
  },
  name: {
    fontSize: 18,
  },
});

export default StockItem;
