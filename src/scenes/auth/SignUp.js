import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { connect } from 'react-redux';
import { signUp } from '../../actions/authActions';
import { bindActionCreators } from 'redux';

// custom components
import Input from '../../components/commons/Input';
import Button from '../../components/commons/Button';
import Loader from '../../components/commons/Loader';

class SignUp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      username: ''
    }
  }

  _handleOnPress = () => {
    let { email, username, password } = this.state;
    this.props.signUp(email, username, password)
      .then(() => {
        if (!this.props.hasError)
          this.props.navigation.navigate('SignIn')
      });
  }

  render() {
    let { hasError, isLoading } = this.props;

    if (isLoading) {
      return (
        <Loader />
      )
    } else {
      return (
        <View style={styles.container}>
          <View style={styles.header}>
            <Text style={styles.title}>
              FreeGo,
            </Text>
            <Text style={styles.subtitle}>
              Inscription
            </Text>
          </View>
          <View style={styles.form}>
            <Input
              placeholder="Pseudo"
              type="username"
              onChangeText={(text) => this.setState({ username: text })}
            />
            <Input
              placeholder="Email"
              type="emailAddress"
              onChangeText={(text) => this.setState({ email: text })}
            />
            <Input
              placeholder="Mot de passe"
              type="password"
              secure={true}
              onChangeText={(text) => this.setState({ password: text })}
            />
            <Button
              text="S'inscrire"
              onPress={this._handleOnPress}
            />
            {hasError && <Text style={styles.error}>Une erreur est survenue</Text>}
          </View>
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 20,
    marginRight: 20,
  },
  header: {
    flex: 2,
    justifyContent: 'center'
  },
  form: {
    flex:3
  },
  title: {
    fontSize: 28,
  },
  subtitle: {
    fontSize: 22,
  },
  error: {
    color: 'red',
    marginTop: 10
  }
});

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators({ signUp }, dispatch)
  }
};

const mapStateToProps = (state) => {
  return {
    hasError: state.authReducer.hasError,
    isLoading: state.authReducer.isLoading
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
