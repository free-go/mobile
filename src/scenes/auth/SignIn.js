import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { login } from '../../actions/authActions';

// custom components
import Loader from '../../components/commons/Loader';
import Input from '../../components/commons/Input';
import Button from '../../components/commons/Button';

class SignIn extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
    }
  }

  _login = () => {
    const { email, password } = this.state;
    this.props.login(email.toLowerCase(), password);
  }

  render() {
    const { hasError, isLoading } = this.props;
    if (isLoading) {
      return (
        <Loader />
      )
    } else {
      return (
        <View style={styles.container}>
          <View style={styles.header}>
            <Text style={styles.title}>
              FreeGo,
            </Text>
            <Text style={styles.subtitle}>
              Connexion
            </Text>
          </View>
          <View style={styles.form}>
            <Input
              placeholder="Email"
              type="emailAddress"
              onChangeText={(text) => this.setState({ email: text })}
              onSubmitEditing={this._login}
              maxLength={80}
            />
            <Input
              placeholder="Mot de passe"
              type="password"
              onChangeText={(text) => this.setState({ password: text })}
              onSubmitEditing={this._login}
              maxLength={80}
              secure={true}
            />
            <Button
              text="Se connecter"
              onPress={this._login}
            />
            {hasError && <Text style={styles.error}>Identifiant ou mot de passe incorrect</Text>}
          </View>
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 20,
    marginRight: 20,
  },
  header: {
    flex: 2,
    justifyContent: 'center'
  },
  form: {
    flex:3,
  },
  title: {
    fontSize: 28,
  },
  subtitle: {
    fontSize: 22,
  },
  error: {
    color: 'red',
    marginTop: 10
  }
});

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators({ login }, dispatch)
  }
};

const mapStateToProps = (state) => {
  return {
    hasError: state.authReducer.hasError,
    isLoading: state.authReducer.isLoading
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
