import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

import { connect } from 'react-redux';
import { addStock } from '../../actions/stockActions';
import { bindActionCreators } from 'redux';

// custom components
import Input from '../../components/commons/Input';
import Button from '../../components/commons/Button';

class AddStock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: ''
    }
  }

  _onSubmit = () => {
    let { name } = this.state;
    if (name.length > 1) {
      this.props.addStock(name)
        .then(() => this.props.navigation.navigate('StocksList'));
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Input type="name" placeholder="Nom du frigo" maxLength={80}
          onChangeText={(text) => this.setState({ name: text})}
          onSubmitEditing={this._onSubmit} />
        <Button text="CREER" onPress={this._onSubmit} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 5
  },
});

const mapStateToProps = (state) => {
  return {
    isLoading: state.stockReducer.isLoading,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators({ addStock }, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddStock);
