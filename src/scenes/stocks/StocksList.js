import React, { Component } from 'react';
import { StyleSheet, View, FlatList, ActivityIndicator } from 'react-native';

import { connect } from 'react-redux';
import { getStocks, getStock } from '../../actions/stockActions';
import { bindActionCreators } from 'redux';

// custom components
import Button from '../../components/commons/Button';
import StockItem from '../../components/stocks/StockItem';
import Loader from '../../components/commons/Loader';

class Stocks extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
    this.props.getStocks();
  }

  render() {
    let { stocks, isLoading } = this.props;
    if(isLoading) {
      return (
        <Loader />
      )
    } else {
      return (
        <View style={styles.container}>
          <FlatList data={stocks}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({item}) =>
              <StockItem
                stock={item}
                onPress={() => this.props.getStock(item.id.toString())} />}
          />
          <Button text="AJOUTER UN FRIGO" onPress={() => this.props.navigation.navigate('AddStock')} />
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5
  },
});

const mapStateToProps = (state) => {
  return {
    stocks: state.stockReducer.stocks,
    isLoading: state.stockReducer.isLoading,
    currentStock: state.stockReducer.currentStock,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators({ getStocks, getStock }, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Stocks);
