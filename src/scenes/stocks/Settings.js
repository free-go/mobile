import React, { Component } from 'react';
import { StyleSheet, View, FlatList, TouchableOpacity, ActivityIndicator, Text, AsyncStorage } from 'react-native';

import { connect } from 'react-redux';
import { addUser } from '../../actions/stockActions';
import { bindActionCreators } from 'redux';

// custom components
import Input from '../../components/commons/Input';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { colors } from '../../theme/colors';

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: []
    }
  }

  componentDidMount = () => {
    this._getUsers(this.props.idStock);
    let { idStock, currentStock, stocks } = this.props;
  }

  _getUsers = (idStock) => {
    const url = 'http://vps631308.ovh.net';
    AsyncStorage.getItem('token').then(token => {
      return fetch(url + '/stock/' + idStock + '/users', {
        headers: {
          'Authorization': "Bearer " + token,
        }
      })
        .then((res) => res.json())
        .then((json) => this.setState({users: json}))
        .catch(err => console.log(err))
    });
  }

  _userRemove = (userCode) => {
    const idStock = this.props.idStock;
    const url = 'http://vps631308.ovh.net';
    AsyncStorage.getItem('token').then(token => {
      return fetch(url + '/stock/' + idStock + '/user/' + userCode, {
        method: 'DELETE',
        headers: {
          'Authorization': "Bearer " + token,
        }
      })
        .then((res) => console.log(res))
        .catch(err => console.log(err))
    })
      .then(() => this._getUsers(idStock));
  }

  _userAdd = (userCode) => {
    const {idStock} = this.props;
    const url = 'http://vps631308.ovh.net';
    AsyncStorage.getItem('token').then(token => {
      return fetch(url + '/stock/' + idStock + '/adduser', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': "Bearer " + token
        },
        body: JSON.stringify({user_id: userCode})
      })
      .then((res) => console.log(res))
      .catch(err => console.log(err))
    })
      .then(() => this._getUsers(idStock))
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Partagez mon frigo avec:</Text>
        <Input
          placeholder="user code"
          keyboardType="numeric"
          onChangeText={(text) => this.setState({ userCode: text })}
          onSubmitEditing={() => this._userAdd(this.state.userCode)}
          maxLength={13}
        />
        <FlatList style={styles.users} data={this.state.users}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({item}) =>
            <View style={styles.user}>
              <Text style={styles.username}>{item.username}</Text>
              <TouchableOpacity
                style={styles.button}
                activeOpacity={.5}
                onPress={() => this._userRemove(item.id)}>
                  <Icon name='account-remove' size={30} color='white' />
              </TouchableOpacity>
            </View>}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5
  },
  title: {
    fontSize: 18
  },
  username: {
    fontSize: 16,
    color: 'white'
  },
  users: {
    marginTop: 20
  },
  user: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: colors.primary,
    marginBottom: 8,
    padding: 10
  }
});

const mapStateToProps = (state) => {
  return {
    idStock: state.stockReducer.idStock,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators({ addUser }, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
