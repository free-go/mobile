import React, { Component } from 'react';
import { StyleSheet, View, FlatList, Text } from 'react-native';

import { connect } from 'react-redux';
import { removeProduct, getStock, updateQuantity } from '../../actions/stockActions';
import { bindActionCreators } from 'redux';

// custom components
import Loader from '../../components/commons/Loader';
import ProductItem from '../../components/products/ProductItem';

class Stock extends Component {
  constructor(props) {
    super(props);
  }

  _deleteProduct = (idProduct) => {
    let {idStock} = this.props;
    if (idProduct) {
      this.props.removeProduct(idStock, idProduct);
      this.props.getStock(idStock);
    }
  }

  _updateQuantity = (idProduct, newQt) => {
    let {idStock} = this.props;
    if (newQt !== '') {
      this.props.updateQuantity(idStock, idProduct, newQt)
        .then(() => this.props.getStock(idStock))
    }
  }

  render() {
    let { currentStock, isLoading } = this.props;
    console.log(currentStock);
    if(isLoading || (currentStock === null)) {
      return (
        <View style={styles.container}>
          <Loader />
        </View>
      )
    } else {
      if (currentStock.length !== 0) {
        return (
          <View style={styles.container}>
            <FlatList
              data={currentStock}
              renderItem={({item}) => <ProductItem product={item} onUpdate={this._updateQuantity} onDelete={this._deleteProduct}/>}
            />
          </View>
        )
      } else {
        return (
          <View style={styles.container}>
            <Text style={styles.empty}>Votre frigo serait-il vide?</Text>
          </View>
        )
      }
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5
  },
  empty: {
    fontSize: 16,
    textAlign: 'center',
    marginTop: 20
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

const mapStateToProps = (state) => {
  return {
    isLoading: state.stockReducer.isLoading,
    currentStock: state.stockReducer.currentStock,
    idStock: state.stockReducer.idStock
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators({ removeProduct, getStock, updateQuantity }, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Stock);
