import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, Modal, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { colors } from '../../theme/colors';
import { connect } from 'react-redux';
import { addProduct } from '../../actions/stockActions';
import { bindActionCreators } from 'redux';

// custom components
import Button from '../../components/commons/Button';
import Input from '../../components/commons/Input';
import Nutriscore from '../../components/products/Nutriscore';

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      quantity: 0
    }
  }

  _handleOnPressAdd = () => {
    const quantity = this.state.quantity;
    const { idStock } = this.props;
    const product = this.props.navigation.getParam('product', 'NO-PRODUCT');
    this.props.addProduct(idStock, product.id, quantity)
      .then(() => {
        this.setState({modalVisible: false, quantity: 0});
        this.props.navigation.navigate('Scan');
        this.props.navigation.navigate('Stock');
      })
  }

  render() {
    const product = this.props.navigation.getParam('product', 'NO-PRODUCT');
    const { idStock } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.product}>
          <Image
            source={{uri: product.image}}
            style={styles.image}
            resizeMode="contain" />
          <View style={styles.details}>
            <Text style={styles.name}>{product.name}</Text>
            <Text style={styles.brand}>{product.brand}</Text>
            <Nutriscore score={product.nutriscore} style={styles.nutriscore} />
          </View>
        </View>
        <View style={styles.actions}>
          <Button text="AJOUTER A MON FRIGO" onPress={() => this.setState({ modalVisible:true})} />
        </View>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {}} >
          <View>
            <View style={styles.modalHeader}>
              <TouchableHighlight onPress={() => { this.setState({modalVisible: false}) }}>
                <Icon name="close" size={35} color={colors.primary}/>
              </TouchableHighlight>
            </View>
            <View style={styles.modalForm}>
              <Text>Quantité: </Text>
              <Input
                keyboardType="numeric"
                onChangeText={(qt) => this.setState({ quantity: qt }) }
                onSubmitEditing={this._handleOnPressAdd}
                maxLength={5} />
              <Button text="AJOUTER" onPress={this._handleOnPressAdd} />
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 20,
    marginBottom: 20
  },
  product: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  actions: {
    flex: 3,
    marginTop: 20
  },
  image: {
    flex: 1,
  },
  details: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 10,
    paddingLeft: 10,
  },
  name: {
    fontSize: 18,
    fontWeight: '900'
  },
  modalHeader: {
    position: 'absolute',
    right: 0,
    top: 0,
  },
  modalForm: {
    paddingTop: 80,
    paddingHorizontal: 60
  }
});

const mapStateToProps = (state) => {
  return {
    idStock: state.stockReducer.idStock,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators({ addProduct }, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);
