import React, { Component } from 'react';
import { Button, Text, View, AsyncStorage } from 'react-native';
import { RNCamera } from 'react-native-camera';

import { colors } from '../../theme/colors';

class Scan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      camera: {
        type: RNCamera.Constants.Type.back,
        flashMode: RNCamera.Constants.FlashMode.auto,
      }
    };
  }

  onBarCodeRead = (scanResult) => {
    if (scanResult.data != null) {
      barCode = scanResult.data;
      const url = 'http://vps631308.ovh.net/product/barcode/' + barCode;
      AsyncStorage.getItem('token').then(token => {
        return fetch(url, {
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
          .then((res) => {
            if(!res.ok)
              throw Error(res.statusText);
            return res;
          })
          .then((res) => res.json())
          .then((json) => {
            this.props.navigation.navigate('Product', { product: json });
          })
      });
    }
    return;
  }

  render() {
    return (
      <View style={styles.container}>
        <RNCamera
          captureAudio={false}
          flashMode={this.state.camera.flashMode}
          onBarCodeRead={this.onBarCodeRead.bind(this)}
          permissionDialogTitle={'Permission to use camera'}
          permissionDialogMessage={'We need your permission to use your camera phone'}
          style={styles.preview}
          type={this.state.camera.type} />
        <View style={[styles.overlay, styles.topOverlay]}>
          <Text style={styles.scanScreenMessage}>Scan du code barre.</Text>
        </View>
        <View style={[styles.overlay, styles.bottomOverlay]}>
          <Button
            onPress={() => { this.props.navigation.navigate('Search') }}
            style={styles.enterBarcodeManualButton}
            color={colors.primary}
            title="Saisir manuellement" />
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  overlay: {
    position: 'absolute',
    padding: 16,
    right: 0,
    left: 0,
    alignItems: 'center'
  },
  topOverlay: {
    top: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  bottomOverlay: {
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.4)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  enterBarcodeManualButton: {
    padding: 15,
  },
  scanScreenMessage: {
    fontSize: 14,
    color: 'white',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  }
};

export default Scan;
