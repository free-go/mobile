import React, { Component } from 'react';
import { StyleSheet, Alert, Image, Text, View, AsyncStorage } from 'react-native';

// custom components
import Loader from '../../components/commons/Loader';
import Input from '../../components/commons/Input';

import { BARCODE } from '../../theme/images';
import { colors } from '../../theme/colors';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchedText: '',
      isLoading: false,
    }
  }

  _getProductByBarCode = (barCode) => {
    const url = 'http://vps631308.ovh.net/product/barcode/' + barCode;
    this.setState({isLoading: true});
    AsyncStorage.getItem('token').then(token => {
      return fetch(url, {
        headers: {
          'Authorization': 'Bearer ' + token
        }
      })
        .then((res) => res.json())
        .then((json) => {
          this.setState({ isLoading: false, });
          this.props.navigation.navigate('Product', { product: json });
        })
        .catch((err) => {
          Alert.alert('Code barre invalide');
          this.setState({ isLoading: false, searchedText: ''});
        });
    });
  }

  _displayLoading = () => {
    if (this.state.isLoading) {
      return (
        <Loader />
      )
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={BARCODE} style={styles.image} resizeMode="contain" />
        <View style={styles.search}>
          <Input
            placeholder="Bar code"
            keyboardType="numeric"
            onChangeText={(text) => this.setState({ searchedText: text })}
            onSubmitEditing={() => this._getProductByBarCode(this.state.searchedText)}
            maxLength={13}
          />
          <View style={styles.result}>
            {this._displayLoading()}
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex:1,
    height: undefined,
    width: undefined,
  },
  search: {
    flex:3,
    marginLeft: 20,
    marginRight: 20,
  },
  result: {
    flex: 1,
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
});

export default Search;
