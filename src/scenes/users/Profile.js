import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { connect } from 'react-redux';
import { logout } from '../../actions/authActions';
import { bindActionCreators } from 'redux';

import { colors } from '../../theme/colors';
import tokenToId from '../../utils/index';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: null
    }
  }

  componentDidMount = () => {
    AsyncStorage.getItem('token')
      .then((token) => {
        let userId = tokenToId(token);
        this.setState({userId: userId});
      })
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => { this.props.logout() }} style={styles.logout}>
          <Text>Se déconnecter</Text>
          <Icon name="logout" size={35} color={colors.primary}/>
        </TouchableOpacity>
        <Text style={styles.code}>Vous souhaitez partager un frigo avec un autre utilisateur ? Transmettez lui votre code utilisateur: {this.state.userId}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 5
  },
  logout: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom: 20
  },
  code: {
    textAlign: 'justify',
    fontSize: 16
  }
});

const mapStateToProps = (state) => {
  return { }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators({ logout }, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
