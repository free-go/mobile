import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

// scenes
import SignIn from '../scenes/auth/SignIn';
import SignUp from '../scenes/auth/SignUp';

import { colors } from '../theme/colors';

const AuthTabNavigator = createMaterialBottomTabNavigator({
    SignIn: {
      screen: SignIn,
      navigationOptions: {
        tabBarIcon: () => {
          return <Icon name='login' size={30} color='white' />
        }
      }
    },
    SignUp: {
      screen: SignUp,
      navigationOptions: {
        tabBarIcon: () => {
          return <Icon name='account-plus' size={30} color='white' />
        }
      }
    }
  },
  {
    initialRouteName: 'SignIn',
    barStyle: { backgroundColor: colors.primary },
    labeled: false,
  }
);

export default createAppContainer(AuthTabNavigator);
