import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// scenes
import Stock from '../scenes/stocks/Stock';
import Search from '../scenes/products/Search';
import Profile from '../scenes/users/Profile';
import StocksList from '../scenes/stocks/StocksList';
import Settings from '../scenes/stocks/Settings';
import Scan from '../scenes/products/Scan';
import Product from '../scenes/products/Product';

import { colors } from '../theme/colors';

const ScanStack = createStackNavigator({
    Scan: {
      screen: Scan,
      navigationOptions: {
        header:null
      }
    },
    Search: Search,
    Product: Product
  }
);

const StockNavigation = createMaterialBottomTabNavigator({
    StocksList: {
      screen: StocksList,
      navigationOptions: {
        tabBarIcon: () => {
          return <Icon name='fridge' size={20} color='white' />
        }
      }
    },
    Stock: {
      screen: Stock,
      navigationOptions: {
        tabBarIcon: () => {
          return <Icon name='food-variant' size={20} color='white' />
        }
      }
    },
    ScanStack: {
      screen: ScanStack,
      navigationOptions: {
        tabBarIcon: () => {
          return <Icon name='barcode-scan' size={20} color='white' />
        }
      }
    },
    Settings: {
      screen: Settings,
      navigationOptions: {
        tabBarIcon: () => {
          return <Icon name='account-multiple-plus' size={20} color='white' />
        }
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        tabBarIcon: () => {
          return <Icon name='account' size={20} color='white' />
        }
      }
    }
  },
  {
    initialRouteName: 'Stock',
    barStyle: { backgroundColor: colors.primary },
    labeled: false,
  }
);

export default createAppContainer(StockNavigation);
