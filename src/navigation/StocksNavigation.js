import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { createStackNavigator, createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

// scenes
import StocksList from '../scenes/stocks/StocksList';
import AddStock from '../scenes/stocks/AddStock';
import Profile from '../scenes/users/Profile';

import { colors } from '../theme/colors';

const StocksStack = createStackNavigator({
    StocksList: {
      screen: StocksList,
      navigationOptions: {
        title: 'Mes Frigos'
      }
    },
    AddStock: {
      screen: AddStock,
      navigationOptions: {
        title: 'Nouveau frigo'
      }
    },
  },
  {
    initialRouteName: 'StocksList',
    barStyle: { backgroundColor: colors.primary },
    labeled: false,
  }
);

const StocksNavigation = createMaterialBottomTabNavigator({
    StocksStack: {
      screen: StocksStack,
      navigationOptions: {
        tabBarIcon: () => {
          return <Icon name='fridge' size={20} color='white' />
        }
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        tabBarIcon: () => {
          return <Icon name='account' size={20} color='white' />
        }
      }
    }
  },
  {
    initialRouteName: 'StocksStack',
    barStyle: { backgroundColor: colors.primary },
    labeled: false,
  }
);


export default createAppContainer(StocksNavigation);
