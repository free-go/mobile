import React, { Component } from 'react';

import { connect } from 'react-redux';

// custom components of navigation
import AuthNavigation from './navigation/AuthNavigation';
import StockNavigation from './navigation/StockNavigation';
import StocksNavigation from './navigation/StocksNavigation';

class Main extends Component {
  render() {
    const { isLogged, currentStock } = this.props;
    if (isLogged) {
      if (currentStock) {
        return (
          <StockNavigation />
        )
      } else {
        return (
          <StocksNavigation />
        )
      }
    } else {
      return (
        <AuthNavigation />
      )
    }
  }
}

const mapStateToProps = (state) => {
  return {
    isLogged: state.authReducer.isLogged,
    currentStock: state.stockReducer.currentStock,
  }
};


export default connect(mapStateToProps)(Main);
