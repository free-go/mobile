import Base64 from './Base64';

// Found userID from the token
export default (token) => {
  const tokenArray = token.split('.');
  if (tokenArray.length > 1) {
    const decodedToken = JSON.parse(Base64.atob(tokenArray[1]));
    if (decodedToken) {
      return decodedToken.identity;
    }
  } else {
    return;
  }
}
